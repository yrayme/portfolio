# Mi Portafolio

Este es el repositorio de mi portafolio personal, desarrollado con Next.js 14, Tailwind CSS y TypeScript.

## Curriculum

Puedes encontrar mi curriculum completo en [link a tu curriculum].

## Tecnologías Utilizadas

- Next.js 14
- Tailwind CSS
- TypeScript
- i18next para las traducciones
- i18nexus para cargar las traducciones

## Instalación

1. Clona este repositorio.
2. Instala las dependencias usando `npm install`.
3. Ejecuta el proyecto usando `npm run dev`.

## Contribuciones

Las contribuciones son bienvenidas. Si deseas contribuir a este proyecto, por favor abre un issue primero para discutir los cambios propuestos.
