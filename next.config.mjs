// @ts-check
 
/**
 * @type {import('next').NextConfig}
 */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        hostname: "flagcdn.com"
      }
    ]
  }
  /* config options here */
}
 
export default nextConfig;