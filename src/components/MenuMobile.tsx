import React, { Fragment } from 'react'
import { Dialog, Transition } from '@headlessui/react';
import { useTranslation } from 'react-i18next';
import LangDropdown from './common/LangDropdown';
import Button from './common/Button';
import AllIcons from './common/icons';
import Image from 'next/image';

interface Props {    
    open: boolean;
    setOpen: React.Dispatch<React.SetStateAction<boolean>>;
    handleMenu: (id: string) => void;
}

const MenuMobile: React.FC<Props> = ({ open, setOpen, handleMenu }) => {
    const { t } = useTranslation();

    return (
        <Transition appear show={open} as={Fragment}>
            <Dialog as="div" className="relative z-50" onClose={() => setOpen(!open)}>
                <Transition.Child
                    as={Fragment}
                    enter="transition-opacity ease-linear duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="transition-opacity ease-linear duration-300"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    <div className="fixed inset-0 bg-black opacity-50" />
                </Transition.Child>
                <div className="fixed inset-0" />

                <div className="fixed inset-0 overflow-hidden">
                    <div className="absolute inset-0 overflow-hidden">
                        <div className="pointer-events-none fixed inset-y-0 right-0 flex max-w-full pl-10">
                            <Transition.Child
                                as={Fragment}
                                enter="transform transition ease-in-out duration-500 sm:duration-700"
                                enterFrom="translate-x-full"
                                enterTo="translate-x-0"
                                leave="transform transition ease-in-out duration-500 sm:duration-700"
                                leaveFrom="translate-x-0"
                                leaveTo="translate-x-full"
                            >
                                <Dialog.Panel className="pointer-events-auto border-l-2 border-gray-1 drop-shadow-lg">
                                    <div className="flex flex-col h-screen overflow-hidden bg-colors-white shadow-xl md:w-[600px] w-[300px] md:p-4 p-3"> 
                                        <div className='flex justify-between items-center'>
                                            <div className='cursor-pointer' onClick={() => handleMenu("home")}>
                                                <Image src={"/assets/img/logo.png"} alt='logo' width={60} height={60}/>
                                            </div> 
                                            <div onClick={() => {setOpen(false)}}>
                                                <AllIcons name="CloseIcon" className="h-4 w-4 text-gray-1 cursor-pointer"/>
                                            </div>
                                        </div>
                                        <div className='overflow-y-auto p-2 h-screen text-colors-black'>   
                                            <div className='flex flex-col justify-center h-full gap-5 pl-20'>    
                                                <div className='cursor-pointer' onClick={() => handleMenu("about")}>
                                                <p className='text-base'>{t("about")}</p>
                                                </div>
                                                <div className='cursor-pointer' onClick={() => handleMenu("experience")}>
                                                <p className='text-base'>{t("experience")}</p>
                                                </div>
                                                <div className='cursor-pointer' onClick={() => handleMenu("projects")}>
                                                <p className='text-base'>{t("projects")}</p>
                                                </div>
                                                <Button title={t('resume')} link="https://cv-yamile.netlify.app/"/>
                                            </div>
                                            <div className='absolute bottom-3 right-4'>
                                                <LangDropdown mobile/>
                                            </div>
                                        </div>
                                    </div>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </div>
            </Dialog>
        </Transition>
    )
}

export default MenuMobile;