"use client";
import { useEffect, useState } from "react";
import Image from "next/image";
import { useTranslation } from "react-i18next";
import { useRouter, usePathname } from "next/navigation";
import i18nConfig from "../../../i18nConfig";

type Props = {
  openMenu?: number;
  color?: string;
  mobile?: boolean;
};

type Lang = {
  code: string
  name: string
  flag: string
}


const LangDropdown = ({ color, mobile }: Props) => {
    const { i18n, t } = useTranslation();
    const currentLocale = i18n.language;
    const router = useRouter();
    const currentPathname = usePathname();
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState<Lang>();

    const languages: Lang[] = [
        {
          code: 'es',
          name: t('spanish'),
          flag: 'https://flagcdn.com/48x36/es.png'
        },
        {
          code: 'en',
          name: t('english'),
          flag: 'https://flagcdn.com/48x36/us.png'
        }
    ]

    const handleChange = (value: string) => {
        setOpen(false)
        const newLocale = value;

        // set cookie for next-i18n-router
        const days = 30;
        const date = new Date();
        date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
        const expires = date.toUTCString();
        document.cookie = `NEXT_LOCALE=${newLocale};expires=${expires};path=/`;

        // redirect to the new locale path
        if (
            currentLocale === i18nConfig.defaultLocale 
        ) {
            router.push('/' + newLocale + currentPathname);
        } else {
            router.push(
                currentPathname.replace(`/${currentLocale}`, `/${newLocale}`)
            );
        }

        router.refresh();
    };

    useEffect(() => {
        const locale = languages.filter(lang => lang.code == currentLocale)[0];
        setValue(locale)
    }, []);

    return (
        <div className="relative inline-block text-left">
            <button
                type="button"
                onClick={() => setOpen(!open)}
                className="flex items-center justify-center"
            >
                {value?.flag && (
                    <Image
                        src={value?.flag}
                        width={100}
                        height={100}
                        alt="flag"
                        className="h-8 w-8 rounded-full border-primary border-2"
                    />
                )}
            </button>

            {open && (
                <div
                    className={`${mobile ? "absolute right-0 z-20 mb-2 w-28 bottom-full" : "origin-top-right absolute right-0 mt-2"} ${color} rounded-md shadow-sm drop-shadow-md  w-28 bg-colors-white`}
                >
                    <div
                        className="py-1"
                        role="menu"
                        aria-orientation="vertical"
                        aria-labelledby="options-menu"
                    >
                        {languages.map((lang) => (
                            <div
                                key={lang.code}
                                onClick={() => handleChange(lang.code)}
                                className="flex cursor-pointer px-4 py-2 text-sm gap-2"
                                role="menuitem"
                            >
                                {lang.flag && (
                                    <Image
                                        src={lang.flag}
                                        alt="flag"
                                        className="h-6 w-6 rounded-full"
                                        width={50}
                                        height={50}
                                    />
                                )}
                                <p className="text-sm">{lang.name}</p>
                            </div>
                        ))}
                    </div>
                </div>
            )}
        </div>
    );
};

export default LangDropdown;