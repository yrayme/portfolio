'use client';
import React, { useState } from 'react';
import { Tab } from '@headlessui/react'
import { useTranslation } from 'react-i18next';
import { Fade } from 'react-awesome-reveal';

type workProps = {
    id: number;
    work: string;
    title: string;
    textlink?: string;
    link?: string;
    year: string;
    description: {
        id: number;
        text:string;
    }[];
}

const TabsExperience = () => { 
    const { t } = useTranslation();
    let [ works ] = useState<workProps[]>([
        {
            id: 1,
            work: t('onlinesat'),
            title: t('title-sat'),
            textlink: t("title-link-sat"),
            link: "https://www.linkedin.com/company/online-sat-inc/",
            year: t('year-sat'),
            description: [
                {
                    id: 1,
                    text: t('description-sat')
                },
                {
                    id: 2,
                    text: t('description1-sat')
                },
                {
                    id: 3,
                    text: t('description2-sat')
                },
                {
                    id: 4,
                    text: t('description3-sat')
                },
            ]
        },
        {
            id: 2,
            work: t('shokworks'),
            title: t('title-shok'),
            textlink: t("title-link-shok"),
            link: "https://www.linkedin.com/company/shokworks/",
            year: t('year-shok'),
            description: [
                {
                    id: 1,
                    text: t('description-shok')
                },
                {
                    id: 2,
                    text: t('description1-shok')
                },
                {
                    id: 3,
                    text: t('description2-shok')
                },
                {
                    id: 4,
                    text: t('description3-shok')
                },
            ]
        },
        {
            id: 3,
            work: t('vilo'),
            title: t('title-vilo'),
            year: t('year-vilo'),
            description: [
                {
                    id: 1,
                    text: t('description-vilo')
                },
                {
                    id: 2,
                    text: t('description1-vilo')
                },
                {
                    id: 3,
                    text: t('description2-vilo')
                },
                {
                    id: 4,
                    text: t('description3-vilo')
                },
            ]
        },
        {
            id: 4,
            work: t('kmg'),
            title: t('title-kmg'),
            year: t('year-kmg'),
            description: [
                {
                    id: 1,
                    text: t('description-kmg')
                },
                {
                    id: 2,
                    text: t('description1-kmg')
                },
                {
                    id: 3,
                    text: t('description2-kmg')
                },
                {
                    id: 4,
                    text: t('description3-kmg')
                },
                {
                    id: 5,
                    text: t('description4-kmg')
                },
            ]
        },
        {
            id: 5,
            work: t('tysanmca'),
            title: t('title-ty'),
            year: t('year-ty'),
            description: [
                {
                    id: 1,
                    text: t('description-ty')
                },
                {
                    id: 2,
                    text: t('description1-ty')
                },
                {
                    id: 3,
                    text: t('description2-ty')
                },
            ]
        }
    ])
    const [selectedTab, setSelectedTab] = useState(works[0]);

    return (
        <div className="py-10 w-full max-w-md lg:max-w-full flex justify-center text-colors-black">
            <Tab.Group>
                <div className="lg:flex lg:gap-10 gap-0 overflow-x-auto sm:overflow-hidden">
                    <Tab.List className="flex lg:flex-col lg:space-y-2 space-x-1 lg:border-l-2 lg:border-b-0 lg:pl-3">
                        {works.map((work, idx) => (
                            <Tab
                                key={idx}
                                className={({ selected }) => `w-full py-2.5 text-base lg:text-lg font-medium leading-5 px-5 focus:outline-none ${selected ? 'border-2 border-primary text-primary' : 'text-blue-700'}`}
                                
                                onClick={() => setSelectedTab(work)}
                            >
                                
                                <Fade direction='left' triggerOnce>{work.work}</Fade>
                            </Tab>
                        ))}
                    </Tab.List>
                    <div className='flex flex-col gap-2 ld:mt-0 mt-6'>
                        <Fade direction='up' triggerOnce>
                            <div className='flex flex-wrap gap-2 items-end'>
                                <h2 className="text-lg lg:text-xl font-bold">{selectedTab.title}</h2>
                                <a href={selectedTab.link} target='_blank' className='text-secondary'>{selectedTab.textlink}</a>
                            </div>
                            <p className='text-sm'>{selectedTab.year}</p>
                            <ul className='mt-2 list-outside list-disc'>
                                {selectedTab && selectedTab.description.map((text) => (
                                    <li
                                        key={text.id}
                                        className="relative rounded-md py-1 hover:bg-gray-100"
                                    >
                                        <h3 className="text-base lg:text-lg">
                                            {text.text}
                                        </h3>   
                                    </li>
                                ))}
                            </ul>
                        </Fade>
                    </div>
                </div>
            </Tab.Group>
        </div>
    )
}

export default TabsExperience;
