import React from 'react'
import { useTranslation } from 'react-i18next';

const Footer = () => {
    const { t } = useTranslation();
    return (
        <div className='w-full bg-primary h-20 border-t-2 border-colors-black flex justify-center items-center flex-col gap-1'>
            <p className='text-colors-white font-medium text-base'>{t("footer")}</p>
            <div className='flex flex-wrap gap-1'>
                <p className='text-colors-white font-medium text-sm'>{t("description-figma")}</p>
                <a href='https://www.figma.com/file/dTeIvQRbPGLi9kvFfqbXo2/Portafolio-Yamile-Rayme?type=design&node-id=6-116&mode=design&t=fouk9bGExcXgSqeU-0' target='_blank' rel="noreferrer">
                    <p className='text-secondary font-medium text-sm'>{t("figma")}</p>
                </a>
            </div>
        </div>
    )
}

export default Footer;
