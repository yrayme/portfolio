import React from 'react';

interface Props {
    title: string;
    link?: string;
    onclick?: () => void;
} 

const Button: React.FC<Props>= ({ title, link, onclick }) => {
  return (
    <div className=''>
        <button className='border-2 px-3 py-2 rounded-md border-primary text-primary text-base cursor-pointer hover:mt-2 hover:ml-2 hover:bg-primary hover:text-colors-black hover:opacity-65' onClick={onclick}>
            {link ? (
              <a href={link} target="_blank" rel="noreferrer">{title}</a>
            ): title}
        </button>
    </div>
  )
}

export default Button;
