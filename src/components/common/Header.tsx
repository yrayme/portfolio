'use client';
import Image from 'next/image';
import React, { RefObject, useState } from 'react'
import LangDropdown from './LangDropdown';
import { useTranslation } from 'react-i18next';
import MenuMobile from '../MenuMobile';
import { Fade } from 'react-awesome-reveal';
import Button from './Button';
import AllIcons from './icons';

interface Props {
  ref?: RefObject<HTMLElement> | any;
  handleMenu: (id: string) => void;
}
const Header: React.FC<Props> = ({ ref, handleMenu }) => {
  const { t } = useTranslation();
  const [open, setOpen] = useState(false);

  return (
    <div className='bg-gray-1 w-full px-10'>
      <div className='hidden md:flex items-center justify-between h-[10vh] relative'>
        <Fade direction='down' triggerOnce>
          <div className=''>
              <Image src={"/assets/img/logo.png"} alt='logo' width={60} height={60}/>
          </div>   
        </Fade>
          <div className='flex gap-14 items-center'>
            <Fade direction='down' triggerOnce>
              <div className='flex gap-14 items-center'>
                <div className='cursor-pointer' onClick={() => handleMenu("about")}>
                  <p className='text-base'>{t("about")}</p>
                </div>
                <div className='cursor-pointer' onClick={() => handleMenu("experience")}>
                  <p className='text-base'>{t("experience")}</p>
                </div>
                <div className='cursor-pointer' onClick={() => handleMenu("projects")}>
                  <p className='text-base'>{t("projects")}</p>
                </div>
                <Button title={t('resume')} link="https://cv-yamile.netlify.app/"/>
              </div>
            </Fade>
            <div className='z-50'>
              <LangDropdown/>
            </div>
          </div>
      </div>
      <div className='flex md:hidden items-center justify-between h-16'>
        <div className=''>
            <Image src={"/assets/img/logo.png"} alt='logo' width={40} height={40}/>
        </div>    
        <div className='' onClick={() => setOpen(true)}>
            <AllIcons name="MenuIcon" className='h-5 w-5 text-black cursor-pointer'/>
        </div>
      </div>
      {open && (<MenuMobile open={open} setOpen={setOpen} handleMenu={handleMenu}/>)}
    </div>
  )
}

export default Header;
