import * as React from "react"
import { SVGProps } from "react"

const MenuIcon = (props: SVGProps<SVGSVGElement>) => {    
    return ( 
       <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 30 30"
            fill="none"
            {...props}
        >
            <path
                fill="currentColor"
                d="M27.632 24.706c.608 0 1.193.262 1.633.731.44.47.702 1.11.731 1.789a2.848 2.848 0 0 1-.574 1.858c-.398.515-.958.838-1.563.904l-.227.012H2.368a2.242 2.242 0 0 1-1.633-.731 2.792 2.792 0 0 1-.731-1.789 2.847 2.847 0 0 1 .574-1.859c.398-.514.958-.837 1.563-.903l.227-.012h25.264Zm0-12.353c.628 0 1.23.279 1.674.775.444.497.694 1.17.694 1.872 0 .702-.25 1.375-.694 1.872-.444.496-1.046.775-1.674.775H2.368c-.628 0-1.23-.279-1.674-.775A2.813 2.813 0 0 1 0 15c0-.702.25-1.375.694-1.872.444-.496 1.046-.775 1.674-.775h25.264Zm0-12.353c.628 0 1.23.279 1.674.775.444.497.694 1.17.694 1.872 0 .702-.25 1.375-.694 1.872-.444.496-1.046.775-1.674.775H2.368c-.628 0-1.23-.279-1.674-.775A2.813 2.813 0 0 1 0 2.647C0 1.945.25 1.272.694.775 1.138.28 1.74 0 2.368 0h25.264Z"
            />
        </svg>
    )
   
}


export default MenuIcon