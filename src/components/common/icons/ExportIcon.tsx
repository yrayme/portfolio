import * as React from "react"
import { SVGProps } from "react"

const ExportIcon = (props: SVGProps<SVGSVGElement>) => {    
    return ( 
       <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="none"
            {...props}
        >
            <path
                fill="currentColor"
                stroke="currentColor"
                d="M1.688 1.824h-.5v16.375h16.375v-7.688h.687v8.126a.25.25 0 0 1-.25.25H.75a.25.25 0 0 1-.25-.25V1.387a.25.25 0 0 1 .25-.25h8.125v.687H1.688Zm14.108 1.208.354-.354-.354-.353-.767-.767 3.171-.371-.371 3.17-.772-.771-.354-.354-.353.354-5.784 5.784-.551-.552 5.781-5.786ZM9.794 9.039Z"
            />
        </svg>
    )
   
}


export default ExportIcon;