import React from 'react'
import MenuIcon from './MenuIcon';
import CloseIcon from './CloseIcon';
import WhatsappIcon from './WhatsappIcon';
import NetlifyIcon from './NetlifyIcon';
import GithubIcon from './GithubIcon';
import LinkedinIcon from './LinkedinIcon';
import InstagramIcon from './InstagramIcon';
import FootPrintIcon from './FootPrintIcon';
import ExportIcon from './ExportIcon';

interface allIconsProps {
  name: string;
  className: string;
}
const AllIcons: React.FC<allIconsProps> = ({
  name,
  className
}) => {
  const icons = {
    CloseIcon,
    ExportIcon,
    FootPrintIcon,
    GithubIcon,
    InstagramIcon,
    LinkedinIcon,
    MenuIcon,
    NetlifyIcon,
    WhatsappIcon
  }
  const Icon = icons[name as keyof typeof icons] || icons["MenuIcon"];
  return <Icon className={className}/>;
}
export default AllIcons;
