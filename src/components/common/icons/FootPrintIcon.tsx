import * as React from "react"
import { SVGProps } from "react"

const FootPrintIcon = (props: SVGProps<SVGSVGElement>) => {    
    return ( 
       <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="none"
            {...props}
        >
        <path
            fill="currentColor"
            d="M12 10.414c1.682 0 3.095.992 3.494 2.332.501 1.68.434 3.522 2.639 4.478 1.957.58 2.697 1.307 2.697 3.259 0 1.557-1.532 3.212-3.508 3.42-2.21.288-3.929-.09-5.323-.78-1.393.69-3.114 1.068-5.32.78-1.977-.207-3.509-1.869-3.509-3.42 0-1.91.79-2.718 2.744-3.272 2.404-1.068 2.082-2.94 2.618-4.547.22-.651.678-1.222 1.303-1.628A3.977 3.977 0 0 1 12 10.414Zm11.014-1.16c-.596-.45-3.027 1.307-3.792 2.177a2.543 2.543 0 0 0-.755 1.804c0 1.415 1.164 2.562 2.597 2.562 1.09 0 2.022-.66 2.407-1.597.75-1.685.791-4.004-.457-4.946Zm-22.03 0c-1.246.942-1.205 3.261-.455 4.947a2.595 2.595 0 0 0 2.407 1.595c1.433 0 2.596-1.146 2.596-2.56 0-.704-.289-1.341-.754-1.805-.765-.87-3.196-2.627-3.793-2.177ZM15.202.016c6.053.893 5.628 9.757.782 9.043a3.048 3.048 0 0 1-2.594-2.636c-.16-1.345-.52-6.75 1.812-6.407Zm-6.402 0c2.333-.344 1.972 5.061 1.812 6.406a3.046 3.046 0 0 1-2.593 2.636C3.171 9.774 2.747.908 8.8.016Z"
        />
        </svg>
    )
   
}


export default FootPrintIcon;