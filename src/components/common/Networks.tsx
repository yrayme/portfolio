'use client';
import React from 'react'
import AllIcons from './icons';
import { useTranslation } from 'react-i18next';
type Props = {
    id: number;
    link: string;
    icon: string;
}

const Networks = () => {
    const { t } = useTranslation();
    const networks: Props[] = [
        {
            id: 1,
            link: "https://github.com/yrayme",
            icon: "GithubIcon"
        },
        {
            id: 2,
            link: "https://www.instagram.com/yamilerayme/",
            icon: "InstagramIcon"
        },
        {
            id: 3,
            link: "https://www.linkedin.com/in/yamile-rayme-597b46185/",
            icon: "LinkedinIcon"
        },
        {
            id: 4,
            link: "https://api.whatsapp.com/send?phone=584129569788",
            icon: "WhatsappIcon"
        },
        {
            id: 5,
            link: "https://app.netlify.com/teams/yrayme/overview",
            icon: "NetlifyIcon"
        }
    ]
    return (
        <div>
            <div className='fixed bottom-0 z-20 md:left-16 left-6'>
                <div className='flex gap-6 flex-col items-center'>
                    {networks.map(network => (
                        <div key={network.id}>
                            <a href={network.link} target="_blank" rel="noopener noreferrer">
                                <AllIcons name={network.icon} className='w-6 h-6 hover:text-primary hover:md:text-secondary text-secondary md:text-colors-black'/>
                            </a>
                        </div>
                    ))}
                    <div className="h-48 w-1 border-l-2 border-dotted border-colors-black"></div>
                </div>
            </div>
            <div className='fixed bottom-0 z-50 md:right-0 -right-14'>
                <div className='flex gap-24 flex-col items-center'>
                    <div className='rotate-90'>
                        <p className='text-base font-semibold hover:text-primary hover:md:text-secondary text-secondary md:text-colors-black'>{t('email')}</p>
                    </div>
                    <div className="h-48 w-1 border-l-2 border-dotted border-colors-black"></div>
                </div>
            </div>
        </div>
    )
}
export default Networks;
