'use client';
import React, { useRef } from 'react'
import Networks from './common/Networks'
import Header from './common/Header'
import Presentation from './sections/Presentation'
import About from './sections/About'
import Experience from './sections/Experience'
import Projects from './sections/Projects';
import Footer from './common/Footer';

const Landing = () => {
    const homeRef = useRef<HTMLDivElement>(null); 
    const aboutRef = useRef<HTMLDivElement>(null);  
    const experienceRef = useRef<HTMLDivElement>(null);  
    const projectsRef = useRef<HTMLDivElement>(null);  
    
    const menu = (id: string) => {
        switch (id) {
            case "home":
                homeRef.current && homeRef.current.scrollIntoView({ behavior: 'smooth' });
                break;
            case "about":
                aboutRef.current && aboutRef.current.scrollIntoView({ behavior: 'smooth' });
                break;
            case "experience":
                experienceRef.current && experienceRef.current.scrollIntoView({ behavior: 'smooth' });
                break;
            case "projects":
                projectsRef.current && projectsRef.current.scrollIntoView({ behavior: 'smooth' });
                break;        
            default:
                break;
        }
    }
    return (
        <div className='dark:bg-colors-white'>
            <Header handleMenu={menu}/>
            <Networks/>
            <div className='h-[90vh]' ref={homeRef}>
                <Presentation handleMenu={menu}/>
            </div>
            <div className='h-full lg:h-screen bg-background' ref={aboutRef}>
                <About/>
            </div>
            <div className='h-full lg:h-screen bg-background ' ref={experienceRef}>
                <Experience/>
            </div>
            <div className='h-full bg-background' ref={projectsRef}>
                <Projects/>
            </div>
            <Footer/>
        </div>
    )
}

export default Landing;
