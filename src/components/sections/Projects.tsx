import Image from 'next/image';
import React, { useState } from 'react'
import { Fade } from 'react-awesome-reveal';
import { useTranslation } from 'react-i18next';
import AllIcons from '../common/icons';

type projectsProps = {
    id: number;
    title: string;
    description: string;
    technologies: string[];
    netlify: string;
    github: string;
    img: string;
    left: boolean;
}

const Projects = () => {
    const { t } = useTranslation(); 
    let [projects] = useState<projectsProps[]>([
        {
            id: 1,
            title: t('servicar'),
            description: t("description-car"),
            technologies: [t('javascript'), t('firebase'), t('react'), t('redux'),  t('material')],
            netlify: "https://car-rental-yamile.netlify.app",
            github: "https://gitlab.com/yrayme/renta_de_carros",
            img: "/assets/img/car.svg",
            left: false
        },
        {
            id: 2,
            title: t('tickets'),
            description: t("description-ticket"),
            technologies: [t('javascript'), t('html'), t('css')],
            netlify: "https://landing-boletos.netlify.app",
            github: "https://github.com/yrayme/landing-boletos",
            img: "/assets/img/ticket.svg",
            left: true
        },
        {
            id: 3,
            title: t('pokemon'),
            description: t("description-poke"),
            technologies: [t('material'), t('axios'), t('javascript'), t('redux'),  t('react')],
            netlify: "https://pokemon-api-yamile.netlify.app",
            github: "https://github.com/yrayme/admission-test",
            img: "/assets/img/poke.svg",
            left: false
        },
        {
            id: 4,
            title: t('cv'),
            description: t("description-cv"),
            technologies: [t('react'), t('javscript'), t('material'), t('css')],
            netlify: "https://cv-yamile.netlify.app",
            github: "https://gitlab.com/yrayme/curriculum-vitae",
            img: "/assets/img/cv.svg",
            left: true
        }
    ]);

    return (
        <div className='flex w-full items-center h-full flex-col relative'>
            <div>
                <Image src={'/assets/img/footprint-pink.png'} alt='photo' width={200} height={100} className='absolute top-10 left-0 lg:w-64 w-36' />
                <Image src={'/assets/img/footprint-pink.png'} alt='photo' width={100} height={100} className='absolute bottom-20 right-0 lg:w-64 w-36 -rotate-180' />
            </div>
            <div className='flex flex-col gap-6 lg:w-[65%] w-[80%] mt-10'>
                <div className='flex justify-center items-center gap-4 w-full'>
                    <Fade direction='up' cascade><div className='h-1 bg-primary rounded-md lg:w-[200px] w-[100px]'></div></Fade>
                    <Fade direction='up' cascade> <p className='text-primary text-2xl font-semibold text-center'>{t('projects')}</p></Fade>
                    <Fade direction='down' cascade><div className='h-1 bg-primary rounded-md lg:w-[200px] w-[100px]'></div></Fade>
                </div>     
                <div className='flex flex-col justify-center items-center gap-4 md:gap-10 w-full mt-10 md:mt-20'>
                    {projects.map((proj, idx) => (
                        <div key={idx} className={`flex ${proj.left ? 'md:flex-row' : 'md:flex-row-reverse'} flex-col w-full relative md:bg-colors-transparent bg-primary p-4 md:p-0`}>
                            <div className={`flex justify-center ${!proj.left ? "md:items-end" : "md:items-start"} flex-col gap-3 w-full md:w-1/2`}>
                                <Fade direction='up' triggerOnce>
                                    <h2 className='text-lg lg:text-xl font-bold'>{proj.title}</h2>
                                </Fade>
                                <div className={`relative md:bg-primary md:opacity-80 md:p-4 drop-shadow-lg ${proj.left ? 'md:-mr-8 mr-0' : 'md:-ml-8 ml-0'} z-20`}>
                                    <Fade direction={proj.left ? "left" : "right"} triggerOnce>
                                        <p className='text-colors-black text-base'>{proj.description}</p>
                                    </Fade>
                                </div>
                                <Fade direction={proj.left ? "left" : "right"} triggerOnce>
                                    <div className='flex gap-4 flex-wrap'>
                                        {proj.technologies.map(tech => (
                                            <div key={tech}>
                                                <p className='text-sm font-semibold'>{tech}</p>
                                            </div>
                                        ))}
                                    </div>
                                </Fade>
                                <Fade direction={proj.left ? "left" : "right"} triggerOnce>
                                    <div className='flex gap-4 items-center md:justify-center'>
                                        <a href={proj.github} target='_blank' className='cursor-pointer'>
                                            <AllIcons name='GithubIcon' className='w-5 h-5 text-colors-purple-700'/>
                                        </a>
                                        <a href={proj.netlify} target='_blank' className='cursor-pointer mt-1'>
                                            <AllIcons name='ExportIcon' className='w-5 h-5 md:text-primary text-colors-black'/>
                                        </a>
                                    </div>
                                </Fade>
                            </div>
                                <div className='w-full md:w-1/2 md:mt-0 mt-4'>
                                    <Fade direction={proj.left ? "left" : "right"} triggerOnce>
                                        <Image src={proj.img} alt={proj.title} width={570} height={308} className='w-full h-full drop-shadow-xl z-10'/>
                                    </Fade>
                                </div>
                        </div>
                    ))}
                </div>     
                <div className='my-10 text-center'>
                    <p className='text-lg text-primary font-semibold'>{t('more-projects')}</p>
                </div>      
            </div>
        </div>
    )
}

export default Projects;
