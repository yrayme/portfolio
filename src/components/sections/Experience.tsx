'use client';
import Image from 'next/image';
import React from 'react'
import { Fade } from 'react-awesome-reveal';
import { useTranslation } from 'react-i18next';
import TabsExperience from '../common/TabsExperience';

const Experience = () => {
    const { t } = useTranslation(); 
    return (
        <div className='flex w-full items-center h-full flex-col relative'>
            <div>
                <Image src={'/assets/img/footprint-blue.png'} alt='photo' width={200} height={100} className='absolute top-10 left-0 lg:w-64 w-36' />
                <Image src={'/assets/img/footprint-blue.png'} alt='photo' width={100} height={100} className='absolute bottom-20 right-0 lg:w-64 w-36 -rotate-180' />
            </div>
            <div className='flex flex-col h-full justify-center gap-6 lg:w-[65%] w-[80%] lg:mt-0 mt-10'>
                <div className='flex justify-center items-center gap-4 w-full mt-10 lg:mt-0'>
                    <Fade direction='up' cascade><div className='h-1 bg-secondary rounded-md lg:w-[200px] w-[100px]'></div></Fade>
                    <Fade direction='up' cascade> <p className='text-secondary text-2xl font-semibold text-center'>{t('work')}</p></Fade>
                    <Fade direction='down' cascade><div className='h-1 bg-secondary rounded-md lg:w-[200px] w-[100px]'></div></Fade>
                </div>
                <div className='flex justify-center'>
                    <TabsExperience/>
                </div>
            </div>
        </div>
    )
}

export default Experience;
