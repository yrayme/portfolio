'use client';
import Image from 'next/image';
import React from 'react'
import { Fade } from 'react-awesome-reveal';
import { useTranslation } from 'react-i18next';

const About = () => {
    const { t } = useTranslation(); 
    return (
        <div className='flex w-full items-center h-full flex-col relative'>
            <div>
                <Image src={'/assets/img/footprint-pink.png'} alt='photo' width={200} height={100} className='absolute top-10 left-0 lg:w-64 w-36' />
                <Image src={'/assets/img/footprint-pink.png'} alt='photo' width={100} height={100} className='absolute bottom-20 right-0 lg:w-64 w-36 -rotate-180' />
            </div>
            <div className='flex flex-col h-full justify-center gap-6 lg:w-[65%] w-[80%] lg:mt-0 mt-10'>
                <div className='grid lg:grid-cols-2 grid-cols-1 lg:gap-4'>
                    <div className='flex justify-center items-center'>
                        <Fade direction='down' cascade>
                            <div>
                                <Image src={'/assets/img/photo.svg'} alt='photo' width={1919} height={3124} className='lg:w-96 w-60 lg:h-[45vh] h-[30vh]' />
                            </div>
                        </Fade>
                    </div>
                    <div>
                        <div className='flex justify-center items-center gap-4 w-full mt-10 lg:mt-0'>
                            <Fade direction='up' cascade><div className='h-1 bg-primary rounded-md w-[100px]'></div></Fade>
                            <Fade direction='up' cascade> <p className='text-primary text-2xl font-semibold'>{t('about')}</p></Fade>
                            <Fade direction='down' cascade><div className='h-1 bg-primary rounded-md w-[100px]'></div></Fade>
                        </div>
                        <div className='mt-8 text-colors-black'>
                            <Fade direction='right' triggerOnce>
                                <div className='w-full'>
                                    <p className='text-base lg:text-lg'>{t('about-description')}</p>
                                </div>
                            </Fade>
                            <Fade direction='left' triggerOnce>
                                <div className='w-full mt-3'>
                                    <p className='text-base lg:text-lg'>{t('about-description1')}</p>
                                </div>
                            </Fade>
                            <Fade direction='right' triggerOnce>
                                <div className='w-full mt-3'>
                                    <p className='text-base lg:text-lg'>{t('about-description2')}</p>
                                </div>
                            </Fade>
                            <Fade direction='left' triggerOnce>
                                <div className='w-full mt-6 flex items-center justify-center'>
                                    <Image src={'/assets/img/skills.svg'} alt='photo' width={441} height={140} className='w-[80%] h-32' />
                                </div>
                            </Fade>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default About;
