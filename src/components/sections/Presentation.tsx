'use client';
import Image from 'next/image';
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next';
import { Fade } from "react-awesome-reveal";
import Button from '../common/Button';
import AllIcons from '../common/icons';
interface Props {
    handleMenu: (id: string) => void;
  }
const Presentation: React.FC<Props> = ({ handleMenu }) => {
    const { t } = useTranslation();

    return (
        <div className='flex w-full items-center h-full flex-col relative'>     
            <div className='hidden lg:flex'>       
                <div className="absolute bottom-[65vh] right-[600px] z-50">
                    <AllIcons name='FootPrintIcon' className='h-24 w-24 -rotate-90 text-secondary animate-ping'/>
                </div>       
                <div className="absolute bottom-[45vh] left-[600px] z-50">
                    <AllIcons name='FootPrintIcon' className='h-24 w-24 rotate-90 text-secondary animate-ping'/>
                </div>       
                <div className="absolute bottom-[45vh] right-24 z-50">
                    <AllIcons name='FootPrintIcon' className='h-24 w-24 -rotate-90 text-secondary animate-ping'/>
                </div> 
                <div className="absolute top-10 left-24">
                    <AllIcons name='FootPrintIcon' className='h-28 w-24 rotate-90 text-secondary opacity-60 animate-ping'/>
                </div>
            </div>
            <div className='md:w-[65%] w-[80%] h-[80vh] flex z-20'>
                <div className='flex flex-col md:gap-6 gap-4 justify-center text-colors-black'>
                    <Fade direction='left' cascade damping={0.5}>
                        <p className='md:text-lg text-base text-primary font-medium'>{t("hi")}</p>
                        <div className='bg-secondary p-4 md:w-[400px] w-full text-center'>
                            <p className='md:text-5xl text-2xl font-semibold'>{t("name")}</p>
                        </div>
                        <div className='lg:w-[800px] w-full'>
                            <p className='md:text-lg text-base'>{t("description")}</p>
                        </div>                
                        <div>
                            <Button title={t('check')} onclick={() => handleMenu("projects")}/>
                        </div>
                    </Fade>
                </div>
            </div>
            <div className='w-full absolute bottom-0 drop-shadow-lg shadow-xl'>
                <Image
                    src={"/assets/img/wave.svg"}
                    alt='wave'
                    width={1095}
                    height={228}
                    className='w-full hidden md:flex h-full'
                />
                <Image
                    src={"/assets/img/wave-mobile.png"}
                    alt='wave'
                    width={1095}
                    height={228}
                    className='w-full h-40 flex md:hidden z-10'
                />
            </div>
        </div>
    )
}

export default Presentation;
