import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import i18nConfig from "../../../i18nConfig";
import TranslationsProvider from "@/components/TranslationsProvider";
import initTranslations from "../i18n";

const inter = Inter({ subsets: ["latin"] });
export const openGraphImage = { images: '/assets/img/logo.png' }
export const metadata: Metadata = {
  metadataBase: new URL('https://acme.com'),
  title: 'Yamile',
  openGraph: {
    images: '/assets/img/logo.png' 
  },
};

export function generateStaticParams() {
  return i18nConfig.locales.map(locale => ({ locale }));
}
type Props = {
  children: React.ReactNode;
  params: {
    locale: string;
  };
}
const i18nNamespaces = ['landing'];
export default async function RootLayout({
  children,
  params: { locale }
}: Readonly<Props>) {
  const { resources } = await initTranslations(locale, i18nNamespaces);
  return (
    <html lang={locale}>
      <body className={inter.className}>   
        <TranslationsProvider
          namespaces={i18nNamespaces}
          locale={locale}
          resources={resources}
        >
          {children}
        </TranslationsProvider>
      </body>
    </html>
  );
}
