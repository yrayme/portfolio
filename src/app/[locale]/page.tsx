import styles from './page.module.css';
import initTranslations from '../i18n';
import Header from '@/components/common/Header';
import Presentation from '@/components/sections/Presentation';
import About from '@/components/sections/About';
import Networks from '@/components/common/Networks';
import Experience from '@/components/sections/Experience';
import Landing from '../../components/Landing';

interface HomeProps {
  params: {
    locale: string;
  };
}
const i18nNamespaces = ['landing'];

const Home: React.FC<HomeProps> = async({ params: { locale } }) => {
  const { t } = await initTranslations(locale, i18nNamespaces);
  return (
      <main className="lg:overflow-x-hidden"> 
        <Landing/>
      </main>
  );
}

export default Home;
